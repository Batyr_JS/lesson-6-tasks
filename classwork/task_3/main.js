let sec=0;
let IntervalF;
let IntervalB;

let myInterval;
let display=document.getElementById('display');
let start=document.getElementById('start');
let stop=document.getElementById('stop');
let reverse=document.getElementById('reverse');
let reset=document.getElementById('reset');


start.addEventListener('click',function(){
    setIntF();
    clearInt(IntervalB);
});

stop.addEventListener('click',function(){
    clearInt(IntervalF);
    clearInt(IntervalB);
    display.innerHTML='pause';
});

reverse.addEventListener('click',function(){
   setIntB();
   clearInt(IntervalF);
});

reset.addEventListener('click',function(){
    sec=0;
    clearInt(IntervalF);
    clearInt(IntervalB);
    display.innerHTML="Timer has just been reset";
})


function runTimeForward(){
    if(sec<30){
    sec++;
    display.innerHTML=sec;
    }
    else clearInt(IntervalF);
}

function runTimeBack(){
    if(sec>0){
    sec--;
    display.innerHTML=sec;
    }
    else clearInt(IntervalB);
}

    
function setIntF(){
         IntervalF=setInterval(runTimeForward,1000);
}

function setIntB(){
         IntervalB=setInterval(runTimeBack,1000);
}

function clearInt(a){
         clearInterval(a);
}